module blinkshare
/**
 * Programs for the Sustrainable 2022 summer school, Rijeka, Croatia.
 *
 * Date: 2022-06-31
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks
import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w

delayShareI :: SimpleSDSLens Int
delayShareI = sharedStore "delay" 500

main :: Task Bool
main =         enterDeviceInfo
	>>? \spec->withDevice spec (\dev->
		    liftmTask blink dev
		-|| updateSharedInformation [] delayShareI
	)
where
	enterDeviceInfo :: Task TCPSettings
	enterDeviceInfo = enterInformation [] <<@ Label "Device information"

blink :: Main (MTask v Bool) | mtask, liftsds v
blink = declarePin D4 PMOutput \d4->
	liftsds \delayShareM=delayShareI
	In fun \blinkfun=(\x->
		     getSds delayShareM
		>>~. \wait->delay wait
		>>|. writeD d4 x
		>>|. blinkfun (Not x))
	In {main=blinkfun true}
