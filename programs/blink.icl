module blink
/**
 * Programs for the Sustrainable 2022 summer school, Rijeka, Croatia.
 *
 * Date: 2022-06-31
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks                    // Imports \label{lst:blink:importfrom}
import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w                 // Start the task engine \label{lst:blink:doTasks}

main :: Task Bool                         // Main task \label{lst:blink:main}
main =         enterDeviceInfo
	>>? \spec->withDevice spec (\dev->liftmTask blink dev)
where
	enterDeviceInfo :: Task TCPSettings   // Ask which device to use\label{lst:blink:enterDeviceInfo}
	enterDeviceInfo
	 = enterInformation [] <<@ Label "Device information"

blink :: Main (MTask v Bool) | mtask v    // mTask task \label{lst:blink:blink}
blink = declarePin D4 PMOutput \ledPin-> // Builtin LED of D1 Mini\label{lst:blink:declarePin}
	fun \blinkfun=(\state->              // Recursive function to blink\label{lst:blink:fun}
		     delay (ms 500)              // Wait for 500ms \label{lst:blink:delay}
		>>|. writeD ledPin state         // Switch LED to state\label{lst:blink:writeD}
		>>|. blinkfun (Not state))       // Call with inverse state\label{lst:blink:recursive}
	In {main=blinkfun true}              // Main program \label{lst:blink:mTaskmain}
