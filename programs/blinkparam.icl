module blinkparam
/**
 * Programs for the Sustrainable 2022 summer school, Rijeka, Croatia.
 *
 * Date: 2022-06-31
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks
import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w

main :: Task Bool
main =                 enterDeviceInfo -&&- enterDelayTime
	>>? \(spec, wait)->withDevice spec (\dev->liftmTask (blink wait) dev)
where
	enterDeviceInfo :: Task TCPSettings
	enterDeviceInfo = enterInformation [] <<@ Label "Device information"

	enterDelayTime :: Task Int
	enterDelayTime = enterInformation [] <<@ Label "Time between state change (ms)"

blink :: Int -> Main (MTask v Bool) | mtask v
blink wait = declarePin D4 PMOutput \d4->
	fun \blinkfun=(\x->
		     delay (lit wait)
		>>|. writeD d4 x
		>>|. blinkfun (Not x))
	In {main=blinkfun true}
