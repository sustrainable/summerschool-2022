module tempmon
/**
 * Programs for the Sustrainable 2022 summer school, Rijeka, Croatia.
 *
 * Date: 2022-06-31
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks
import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w

tempShareI :: SimpleSDSLens Real
tempShareI = sharedStore "temp" 0.0

main :: Task Real
main =         enterDeviceInfo
	>>? \spec->withDevice spec (\dev->
		    liftmTask tempmon dev
		-|| viewTemperature
	)
where
	enterDeviceInfo :: Task TCPSettings
	enterDeviceInfo = enterInformation [] <<@ Label "Device information"

	viewTemperature :: Task Real
	viewTemperature = viewSharedInformation [] tempShareI
		<<@ Label "Current temperature (C)"

tempmon :: Main (MTask v Real) | mtask, dht, liftsds v
tempmon = DHT (DHT_SHT (i2c 0x45)) \dht->
	liftsds \tempShareM=tempShareI
	In fun \tempfun=(\()->temperature dht
		>>~. \t->setSds tempShareM t
		>>|. tempfun ()
	) In {main=tempfun ()}
