#include <Wire.h>

//Comment if you use the INA 226 breakout board
#define INA260

#ifdef INA260
#include <Adafruit_INA260.h>
Adafruit_INA260 INA = Adafruit_INA260();
#define READPOWER readPower
#else

#include <INA226_WE.h>
#define I2C_ADDRESS 0x40
INA226_WE INA = INA226_WE(I2C_ADDRESS);
#define READPOWER getBusPower
#endif

void setup() {
	Serial.begin(115200);
	Wire.begin();
#ifdef INA260
	if (!INA.begin()) {
		Serial.println("Couldn't find INA260 chip");
		while(1);
	}
	INA.setAveragingCount(INA260_COUNT_4);
#else
	INA.init();
	INA.setAverage(AVERAGE_4);
#endif
}

void loop() {
	Serial.println("Power(W)");
	for (int i = 0; i < 100; i += 1) {
		Serial.printf("%.3f\n", INA.READPOWER());
		delay(100);
	}
}
