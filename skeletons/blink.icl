module blink
/**
 * Programs for the Sustrainable 2022 summer school, Rijeka, Croatia.
 *
 * Date: 2022-06-31
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks                    // Imports
import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w                 // Start the engine

main :: Task Bool                        // Main task
main =         enterDeviceInfo
	>>? \spec->withDevice spec (\dev->liftmTask blink dev)
where
	enterDeviceInfo :: Task TCPSettings  //Ask the user for the device settings
	enterDeviceInfo = enterInformation [] <<@ Label "Device information"

blink :: Main (MTask v Bool) | mtask v   // mTask task
blink = declarePin D4 PMOutput \d4->     // Builtin LED on the D1 Mini
	fun \blinkfun=(\x->                  // Recursive function to blink
		     delay (ms 500)              // Wait for 500ms
		>>|. writeD d4 x                 // Set the LED to the current state
		>>|. blinkfun (Not x))           // Recursively call with inverse state
	In {main=blinkfun true}              // Main program
