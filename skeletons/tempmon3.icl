module tempmon3
/**
 * Programs for the Sustrainable 2022 summer school, Rijeka, Croatia.
 *
 * Date: 2022-06-31
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks
import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w

tempShareI :: SimpleSDSLens Real
tempShareI = sharedStore "temp" 0.0

main :: Task Real
main =         enterDeviceInfo
	>>? \spec->withDevice spec (\dev->
		    liftmTask tempmon dev
		-|| viewTemperature
	)
where
	enterDeviceInfo :: Task TCPSettings
	enterDeviceInfo = enterInformation [] <<@ Label "Device information"

	viewTemperature :: Task Real
	viewTemperature = viewSharedInformation [] tempShareI
		<<@ Label "Current temperature (C)"

tempmon :: Main (MTask v Real) | mtask, dht, liftsds v & fun (v Real, v Real) v
tempmon = DHT (DHT_SHT (i2c 0x45)) \dht->
	liftsds \tempShareM=tempShareI
	In fun \abs=(\x->If (x >. lit 0.0) x (lit (-1.0) *. x))
	In fun \differsenough=(\(old, new)->abs (old -. new) >. lit 0.5)
	In fun \tempfun=(\oldtemp->temperature dht
		>>*. [IfValue (\newtemp-> /* ??? Add the predicate here using differsenough */)
				\newtemp->setSds tempShareM newtemp]
		>>=. \newtemp->tempfun newtemp
	// We have to provide tempfun with an initial value so we pick 0.0
	) In {main=tempfun (lit 0.0)}
