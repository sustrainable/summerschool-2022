module simplepir
/**
 * Programs for the Sustrainable 2022 summer school, Rijeka, Croatia.
 *
 * Date: 2022-06-31
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks
import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w

movementShareI :: SimpleSDSLens Int
movementShareI = sharedStore "movement" 0

main :: Task Int
main =         enterDeviceInfo
	>>? \spec->withDevice spec (\dev->
		    liftmTask pirtask dev
		-|| viewMovement
	)
where
	enterDeviceInfo :: Task TCPSettings
	enterDeviceInfo = enterInformation [] <<@ Label "Device information"

	viewMovement :: Task Int
	viewMovement = viewSharedInformation [] movementShareI
		<<@ Label "No. of detected movements"

pirtask :: Main (MTask v Int) | mtask, PIR, dht, liftsds v
pirtask = declarePin D3 PMInputPullup \pir->
	liftsds \movementShareM=movementShareI
	In fun \pirfun=(\()->
		     interrupt high pir
		>>|. updSds movementShareM ((+.)(lit 1))
		>>|. pirfun ())
	In {main=pirfun ()}
