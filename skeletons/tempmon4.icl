module tempmon4
/**
 * Programs for the Sustrainable 2022 summer school, Rijeka, Croatia.
 *
 * Date: 2022-06-31
 * Authors: Mart Lubbers & Pieter Koopman (mart@cs.ru.nl, pieter@cs.ru.nl)
 */

import StdEnv, iTasks
import mTask.Interpret
import mTask.Interpret.Device.TCP

Start w = doTasks main w

/*??? Define the movement share here */

tempShareI :: SimpleSDSLens Real
tempShareI = sharedStore "temp" 0.0

main :: Task Int
main =         enterDeviceInfo
	>>? \spec->withDevice spec (\dev->
		    liftmTask tempmon dev
		-|| viewMovement
	)
where
	enterDeviceInfo :: Task TCPSettings
	enterDeviceInfo = enterInformation [] <<@ Label "Device information"

	viewMovement :: Task Int
	viewMovement = (viewSharedInformation [] movementShareI <<@ Label "No. of detected movements")
		-|| (viewSharedInformation [] tempShareI <<@ Label "Temperature")

tempmon :: Main (MTask v Int) | mtask, PIR, dht, liftsds v
tempmon = /* ??? Add the PIR declaration here */
	// Declare the temperature sensor
	DHT (DHT_SHT (i2c 0x45)) \dht->
	// Lift the movement counter SDS from iTask to mTask
	liftsds \movementShareM=movementShareI
	// Lift the temperature SDS from iTask to mTask
	In liftsds \tempShareM=tempShareI
	// Task to detect motion
	In fun \motionfun=(\()->
		     interrupt rising pir
		>>|. updSds movementShareM ((+.)(lit 1))
		/* ??? Measure the temperature here and set the tempShareM sds */
		>>|. motionfun ())
	In fun \abs=(\x->If (x >. lit 0.0) x (lit (-1.0) *. x))
	In fun \differsenough=(\(old, new)->abs (old -. new) >. lit 0.5)
	In fun \tempfun=(\oldtemp->temperature` (BeforeSec (lit 60)) dht
		>>*. [IfValue (\newtemp->differsenough (oldtemp, newtemp))
				\newtemp->setSds tempShareM newtemp]
		>>=. \newtemp->tempfun newtemp)
	In {main= /* ??? Call motionfun here */ /*??? Parallel combinator here */ tempfun (lit 0.0)}
