# Green computing for the IoT material

If there are any questions, don't hesitate to contact Mart Lubbers, either on
slack or by sending an email to mart@cs.ru.nl.

## Setup instructions

### Windows

### Nitrile 

- Open an elevated powershell terminal
- Install chocolateley (https://chocolatey.org/install)
- _RESTART YOUR POWERSHELL TERMINAL_
- Install nitrile by running [this powershell command](https://clean-lang.org/about.html#install) (press windows button)
- _REBOOT YOUR SYSTEM_
- If you now run nitrile in your powershell/cmd terminal you should see the help output.

### Arduino/serial port plotter

- Install the serial port driver for the CH340 chipset ([https://www.wemos.cc/en/latest/ch340_driver.html](https://www.wemos.cc/en/latest/ch340_driver.html))
- Finally, install Arduino's IDE [https://www.arduino.cc/en/software](https://www.arduino.cc/en/software)

### Linux

### Nitrile

- Install `curl`, `jq`, `tar` and `libz3-4` (debian: `sudo apt-get install curl jq tar libz3-4`)
- Install `nitrile` by running [this shell script](https://clean-lang.org/about.html#install)
- Add `~/.nitrile/bin` to your path (e.g. for `bash` by running: `echo 'export PATH="$PATH:~/nitrile/bin"' >> ~/.bashrc`)
- If you now run `nitrile` in your terminal, you should see the help text

### Arduino/serial port plotter

- Install Arduino's IDE [https://www.arduino.cc/en/software](https://www.arduino.cc/en/software) (note that on older debian versions the version in the repo is very old and might not be suitable)
- Make sure you have permission to read the serial port ([https://support.arduino.cc/hc/en-us/articles/360016495679-Fix-port-access-on-Linux](https://support.arduino.cc/hc/en-us/articles/360016495679-Fix-port-access-on-Linux))

### Mac OS

Unfortunately there is no version for Mac OS available.
The only option is to use a linux/windows virtual machine using virtualbox or a similar program.
Make sure to select the correct networking settings as you are required to access other devices on the network (microprocessors).

## Compile and run mTask for the first time:

- Open this directory in your terminal (linux) powershell/cmd (windows)
- To fetch the latest package versions from the registry, run: `nitrile update`
- To fetch the actual dependencies, run: `nitrile fetch`
- To build the example progam, run: `nitrile build --only=blink`
  NB. this initial compilation will also compile all libraries and can take some minutes.
- Now there should be a `blink` (linux) `blink.exe` (windows) executable in the directory in conjunction with a `blink-www` folder, a `blink.bc` file and a `blink.pbc` file

## Compile and run a skeleton for the *n*th time where *n > 1*

- Open the directory in your terminal (linux) powershell/cmd (windows)
- Run: `nitrile build --only=NAME`
- for some `NAME.icl` file in the directory.

## Tools to support you when developing Clean/iTask/mTask applications

- Visual studio code plugin for Clean ([available in the marketplace](https://marketplace.visualstudio.com/items?itemName=TOPSoftware.clean-vs-code))
- Vim plugin for Clean ([https://gitlab.com/clean-and-itasks/vim-clean](https://gitlab.com/clean-and-itasks/vim-clean))
- Cloogle: A search engine for Clean functions (includes iTask and mTask)
