module itask1

import iTasks, _SystemArray

Start w = doTasks nameTask2 w

nameTask :: Task String
nameTask = enterInformation [] <<@ Label "Your name please"

// step on continue

helloTask :: Task String
helloTask = nameTask >>? \name->viewInformation [] ("Hello " +++ name)

// step

greeter :: Task String 
greeter =
    nameTask >>*
        [OnAction (Action "Done")  (ifValue (\name -> size name > 2) \n ->return n)
        ,OnAction (Action "Clear") (always greeter)
        ,OnValue  (ifValue (\name->size name > 9) \n -> return n)
        ]
        >>- \name -> viewInformation [] ("Hello " +++ name)

// gender

:: Gender = Male | Female | Other
derive class iTask Gender

nameAndGender :: Task (String, Gender)
nameAndGender =
    nameTask >>? \name ->
    (enterInformation [] <<@ Label ("Your gender " +++ name)) >>? \gender ->
    viewInformation [] (name, gender)

// parallel

nameAndGender2 :: Task (String, Gender)
nameAndGender2 = (enterInformation [] -&&- enterInformation []) >>? viewInformation []

nameTask2 :: Task String
nameTask2 =
  (nameTask -||- 
  (editChoice [] ["Mart","Pieter"] ?None <<@ Label "Select name")) >>? \name ->
   viewInformation [] name

sharedNames :: Task [String]
sharedNames =
    withShared [] \sds ->
    (updateSharedInformation [] sds <<@ Label "Enter names") -||
    (viewSharedInformation [ViewAs length] sds <<@ Label "Count")

nameGenderSDS :: SimpleSDSLens [(String, Gender)]
nameGenderSDS = sharedStore "sdsIdentfier" initialValue
where initialValue = []

