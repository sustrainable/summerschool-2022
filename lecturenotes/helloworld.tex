\section{Introducing edge computing by an example}

The first program one writes when trying a new programming language is \emph{Hello world!} that simply prints the text to the screen to verify that the compilation and execution process works.
In case of microcontrollers, the builtin LED (a monochrome 1-pixel screen) is often blinked to signify that everything is set up correctly.

\subsection{Traditional edge computing}

Using Arduino's \Cpp{} dialect to create the blink program results in the code seen in \autoref{lst:arduinoBlink}.
Arduino programs are implemented as cyclic executives and hence, each program defines a \cinline{setup} and a \cinline{loop} function.
The \cinline{setup} function is executed only once on boot, the \cinline{loop} function is continuously called afterwards and contains the event loop.
In the blink example, the \cinline{setup} function only contains code for setting the general purpose input/output, GPIO, pin to the correct mode.
The \cinline{loop} function alternates the state of the pin representing the LED between \cinline{HIGH} and \cinline{LOW}, turning the LED off and on respectively.
In between, it waits \qty{500}{\ms} so that the blinking is actually visible for the human eye.

\begin{lstlisting}[language=c,caption={Blinking an LED in Arduino \Cpp.},label={lst:arduinoBlink}]
void setup() {
	pinMode(D2, OUTPUT);
}

void loop() {
	digitalWrite(D2, HIGH);
	delay(500);
	digitalWrite(D2, LOW);
	delay(500);
}
\end{lstlisting}
%
To execute such a program on a microprocessor this \Cpp{} code is first compiled to machine code for that processor.
Next the compiled code is stored in the persistent flash memory, EEPROM, of the microprocessor with help of a small bootloader available on the microprocessor.
After a soft restart the \cinline{setup} function is executed once and the \cinline{loop} function is repeated while as long as the processor runs.
The EEPROM memory has a specified life of 100,000 write cycles.
So, you may need to be careful about how often you write to it.
Many programs can be uploaded and executed in such a memory unit.
As soon as you start updating this type of memory during program execution, it might wear out rather quickly.

\subsection{Task-Oriented edge computing}\label{TOPedge}

The \mTask{} variant of the \emph{Hello World!} look very much like the Arduino variant and is as useful for testing the toolchain.
Therefore, this first assignment is just to verify your \mTask{} installation is working and you are able to compiler and execute \Clean{}/\mTask{} programs.

\lstinputlisting[numbers=left,firstline=1,lastline=1,belowskip=0pt]{../programs/blink.icl}
\lstinputlisting[numbers=left,firstline=9,firstnumber=2,aboveskip=0pt,caption={Blinking an LED in mTask and iTask.},label={lst:mTaskBlink}]{../programs/blink.icl}


All \Clean{} programs start with a module declaration, in this case the module name is \cleaninline{blink}.
To work with the \mTask{} library, some imports are required (see \autoref{lst:blink:importfrom}).
As \mTask{} is embedded in \iTask{}, \iTask's \cleaninline{doTasks} is called on \autoref{lst:blink:doTasks} as the \cleaninline{Start} rule of the program.
This function starts the \iTask{} engine and execute the \cleaninline{main} task that is given as the second argument.
The \cleaninline{main} task is defined at \autoref{lst:blink:main} and first asks the use to enter the device information (see \autoref{lst:blink:enterDeviceInfo}).
With this information, \mTask's \cleaninline{withDevice} function is called that allows the program to interact safely with a connected microcontroller.
Using \cleaninline{liftmTask}, an \mTask{} task is lifted to \iTask{}, i.e., it is compiled to bytecode, sent to the microcontroller and the result is observable in \iTask{}.
The \cleaninline{blink} task is an \mTask{} task defined at \autoref{lst:blink:blink} with the type \cleaninline{Main (MTask v Bool) | mtask v} that can be read as: \emph{An \mTask{} task of the type \emph{Bool} parametric in the view \cleaninline{v} as long as this \cleaninline{v} implements the classes defined by the \cleaninline{mtask} class collection}.
This has to do with the fancy way the \mTask{} language is created.
Do not worry when you do not understand all technical details.

The start expression of every \mTask{} program is wrapped in a \cleaninline{main} record to assure that functions and sensors are only defined at the top level.
First on \autoref{lst:blink:declarePin} the GPIO pin D4, connected to the builtin LED is set to output mode.
As \mTask{} is hosted in a functional programming language, instead of using a loop, a recursive function is used.
\autoref{lst:blink:fun} shows this function (\cleaninline{blinkfun}), that has one boolean argument, the state.
%This function first waits for $500$ milliseconds (\autoref{lst:blink:delay}), then writes the state to the pin to either turn on or turn off the LED (\autoref{lst:blink:writeD}) and finally it calls itself recursively with the inverse of the state (\autoref{lst:blink:recursive}).
This function first waits for \qty{500}{\ms} (\autoref{lst:blink:delay}), then writes the state to the pin to either turn on or turn off the LED (\autoref{lst:blink:writeD}) and finally it calls itself recursively with the inverse of the state (\autoref{lst:blink:recursive}).
When calling this function at \autoref{lst:blink:mTaskmain} with some initial state, it results in a blinking LED when executing.

Installation of all required software and suitable microprocessors to execute the examples and exercises of this chapter is outlined in \autoref{sec:software}.
To measure the power consumption of the microprocessor executing our programs one can use the machinery discussed in \autoref{sec:measruringPower}.


\begin{exercise}[H]
	Compile and run the \cleaninline{blink} module by running (see the readme for instructions).

	\begin{lstlisting}[language=bash]
nitrile build -only=blink
./blink\end{lstlisting}

	When you have connected the device properly, it should connect to either one of the networks and show its address.
	Enter this address in the \cleaninline{enterDeviceInfo} task together with the default \mTask{} port number \num{8123}.
	If you then press \emph{Continue} the light on top of the microcomputer should turn on and off according to the given frequency (\qty{500}{\ms}).
	\caption{Hello world! ({\rm\tt blink.icl})}%}%
	\label{ex:compile_and_run}
\end{exercise}



\begin{exercise}[H]
	Change the \cleaninline{blink} function so that it gets a parameter, i.e.\ the time between state changes as follows:

	\lstinputlisting[firstline=25,lastline=26]{blinkparam.icl}
	
	This does require you to provide this extra argument as well.
	By using the \cleaninline{-&&-} combinator, the \cleaninline{enterDeviceInfo} can be combined with another task that asks the user for a time in \unit{\ms}.
	The result of this line will then be a tuple that can be pattern matched and passed on to the \cleaninline{blink} function as follows:

	\lstinputlisting[firstline=17,lastline=17]{blinkparam.icl}

	Finally, adapt the \cleaninline{delay} task in the \mTask{} task so that it uses \cleaninline{wait} instead of a fixed number of milliseconds.
	\begin{hint}
		\cleaninline{wait} is of type \cleaninline{Int} and not of type \cleaninline{v Int} so you have to lift it to the \mTask{} domain first by \cleaninline{lit}.
	\end{hint}
	\caption{Tailor-made blinking ({\rm\tt blinkparam.icl})}%
	\label{ex:tailor-made_blinking}
\end{exercise}
