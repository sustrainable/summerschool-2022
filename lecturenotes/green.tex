%chktex-file 26
\section{Green computing with \mTask{}}\label{sec:green}
MTask offers abstractions for edge layer-specific details such as the heterogeneity of architectures, platforms and frameworks; peripheral access; and multitasking but also for energy consumption and scheduling.
In \mTask{}, tasks are implemented as a rewrite system, where the work is automatically segmented in small atomic bits and stored as a task tree.
Each cycle, a single rewrite step is performed on all task trees, during rewriting, tasks do a bit of their work and progress steadily, allowing interleaved and seemingly parallel operation.
After a loop, the RTS knows which task is waiting on which triggers and is thus able to determine the next execution time for each task.
Utilising this information, the RTS can determine when it is possible and safe to sleep and choose the optimal sleep mode according to the sleeping time.
For example, the RTS never attempts to sleep during an \IIC{} communication because I/O is always contained \emph{within} a rewrite step.

An \mTask{} program is dynamically transformed to byte code.
This byte code and the initial \mTask{} expression are shipped to an \mTask{} IoT node.
The \mTask{} rewrite engine rewrites the current expression just a single rewrite step at a time at the task level.
Other computations are evaluate eagerly.
When subtasks are composed in parallel, all subtasks are rewritten unless the result of the first rewrite step makes the result of the other tasks superfluous.
This task design ensures that all time critical communication with peripherals is within a single rewrite step.
%As a consequence, we cannot have fair multitasking.
%When a single rewrite step would take forever due to an infinite sequence of function calls, this would block the entire IoT node.
Even infinite sequences rewrite steps, as in the \cleaninline{blink} example, are perfectly fine.
The \mTask{} system does proper tail-call optimizations to facilitate this.

This rewriting of tasks is very convenient; the system can inspect the current state of all \mTask{} expressions after a rewrite step.
The system can detect if sleeping of the current task expression is useful and how long this is possible.
When all current subtasks in the system allow a sufficiently long break, the microprocessor can dynamically be switched to a low energy sleeping mode.
The system will wakeup after the determined interval and continues rewriting of the tasks.
The \cleaninline{delay} primitive is an obvious spot where sleeping is possible.
In the next section we will argue that there many other spots where energy saving by inserting a short delay is possible without changing the behaviour of the system.

\subsection{Temperature monitor}

Edge nodes in the IoT are very suited to read sensors.
A typical example is a temperature sensor.
Such a device communicates with the microprocessor via a dedicated protocol over the General Purpose Input Output, GPIO, pins of the microprocessor.
In this example the temperature is measured with the famous DHT sensor that communicates via the I2C protocol.
We do not need to know any details about the sensor or protocol.
The \cleaninline{DHT} primitive of \mTask{} defines a tailor-made \cleaninline{dht} object.
The task \cleaninline{temperature dht} reads the current value from this object.


\begin{exercise}[H]
	The appointed device contains a SHT30x DHT sensor that communicates with the microprocessor using \IIC{}.
	While task values can be observed directly using the appropriate combinators (\cleaninline{>&>}, \cleaninline{>&*}), writing the value to an SDS is an easier option.

	Create a globally available SDS to store the temperature.
	\hint{In \mTask{}, temperature is measured in \textdegree{}Celcius stored in a \cleaninline{Real}.}

	To view this SDS during operation, create \cleaninline{viewTemperature} task and run that in parallel with the \cleaninline{liftmTask} task:
	\lstinputlisting[firstline=28,lastline=30]{tempmon.icl}

	Finally, implement the temperature monitoring task.
	\lstinputlisting[firstline=32,lastline=38]{tempmon.icl}
	
	\caption{Initial temperature monitor ({\rm\tt tempmon.icl})}%
	\label{ex:initial_temperature_monitor}
\end{exercise}

As you can see, this version of the temperature monitoring application generates a lot of data traffic between the edge node ad the server.
Every update of the local SDS results in an automatic message to the server to keep both versions of the SDS in sync.

\begin{exercise}[H]

	The temperature is not bound to change every millisecond so it's not required to measure it that often.
	
	Adapt the program so that there is a 5 second delay in between the measurements.
	This is done by adding a \cleaninline{delay} somewhere.
	
	\caption{Temperature monitor, second iteration ({\rm\tt tempmon2.icl})}%
	\label{ex:second_temperature_monitor}
\end{exercise}

\subsection{Rewrite intervals}
Some \mTask{} examples contain one or more explicit \cleaninline{delay} primitives, offering a natural place for the node executing it to pause.
However, there are many \mTask{} programs that just specify a repeated set of primitives.
A typical example is the program that reads the temperature for a sensor and sets the system LED if the reading is below some given \cleaninline{goal}.

\begin{lstlisting}[caption={A basic thermostat task.},label={lst:thermostat}]
thermostat :: Main (MTask v Bool) | mtask v
thermostat = DHT I2Caddr \dht->
	{main = rpeat (temperature dht >>~. \temp.
	               writeD builtInLED (goal <. temp))}
\end{lstlisting}

This program repeatedly reads the DHT sensor and sets the on-board LED based on the comparison with the \cleaninline{goal} as fast as possible on the \mTask{} node.
This is a perfect solution as long as we ignore the power consumption.
The \mTask{} machinery ensures that if there are other tasks running on the node, they will make progress.
However, this solution is far from perfect when we take power consumption into account.
In most applications, it is very unlikely that the temperature will change significantly within one minute, let alone within some milliseconds.
Hence, it is sufficient to repeat the measurement with an appropriate interval.

There are various ways to improve this program.
The simplest solution is to add an explicit delay to the body of the repeat loop.
A slightly more sophisticated option is to add a repetition period to the \cleaninline{rpeat} combinator.
The combinator implementing this is called \cleaninline{rpeatEvery}.
Both solutions rely on an explicit action of the programmer.

Fortunately, \mTask{} also contains machinery to do this automatically.
The key of this solution is to associate dynamically an evaluation interval with each task.
The interval $\rewriterate{low}{high}$ indicates that the evaluation can be safely delayed by any number of milliseconds within that range.
Such an interval is just a hint for the {RTS}.
It is not a guarantee that the evaluation takes place in the given interval.
Other parts of the task expression can force an earlier evaluation of this part of the task.
When the system is very busy with other work, the task might even be executed after the upper bound of the interval.
The system calculates the rewrite rates from the current task expression.
This has the advantage that the programmer does not have to deal with them and that they are available in each and every \mTask{} program.

\subsection{Basic tasks}

We start by assigning default rewrite rates to basic tasks.
These rewrite rates reflect the expected change rates of sensors and other inputs.
Basic tasks to one-shot set a value of a sensor or actuator usually have a rate of $\rewriterate{0}{0}$, this is never delayed, e.g.\ writing to a GPIO pin.
Basic tasks that continuously read a value or otherwise interact with a peripheral have default rewrite rates that fit standard usage of the sensor.
\autoref{tbl:rewrite} shows the default values for the basic tasks.
I.e.\ reading SDSs and fast sensors such as sound or light aim for a rewrite every \qty{100}{\ms}, medium slow sensors such as gesture sensors every \qty{1000}{\ms} and slow sensors such as temperature or air quality every \qty{2000}{\ms}.

\begin{table}
	\centering
	\caption{Default rewrite rates of basic tasks.}%
	\label{tbl:rewrite}
	\begin{tabular}{ll}
		\toprule
		task & default interval\\
		\midrule
		reading an {SDS} & $\rewriterate{0}{2000}$\\
		slow sensor & $\rewriterate{0}{2000}$\\
		medium sensor & $\rewriterate{0}{1000}$\\
		fast sensor & $\rewriterate{0}{100}$\\
		\bottomrule
	\end{tabular}
\end{table}

\begin{exercise}[H]
	Adapt the program so that only changes bigger than half a \unit{\celcius} are reported.
	Using the step combinator (\cleaninline{>>*.}) you can observe the value of a task and step when the predicate holds.

	If the predicate does not match, the left hand side of the step combinator is scheduled for execution some other time, depending on the characteristics of the task.
	In case of the temperature sensor, the next execution will be within two seconds.

	\hint{Use functions to do the heavy lifting.
		For example, to take the absolute value over real numbers you can define:
		\lstinputlisting[firstline=35,lastline=35]{tempmon3.icl}
		Furthermore, remember that multiparameter functions are always written with tuple notation.
		So a function that determines if two real numbers differ more than \qty{0.5}{\celcius} is defined as:
		\lstinputlisting[firstline=36,lastline=36]{tempmon3.icl}
	}
	
	\caption{Temperature monitor, third iteration ({\rm\tt tempmon3.icl})}%
	\label{ex:third_temperature_monitor}
\end{exercise}

\subsection{Tweaking rewrite rates}
A tailor-made ADT (see \autoref{lst:interval}) determines the timing intervals for which the value is determined at runtime but the constructor is known at compile time.
During compilation, the constructor of the ADT is checked and code is generated accordingly.
If it is \cleaninline{Default}, no extra code is generated.
In the other cases, code is generated to wrap the task tree node in a \emph{tune rate} node.
In the case that there is a lower bound, i.e.\ the task must not be executed before this lower bound, an extra \emph{rate limit} task tree node is generated that performs a no-op rewrite if the lower bound has not passed but caches the task value.

\begin{lstlisting}[caption={The ADT for timing intervals in mTask.},label={lst:interval}]
:: TimingInterval v
	= Default
	| BeforeMs (v Int)         // yields $\rewriterate{0}{x}$
	| BeforeS  (v Int)         // yields $\rewriterate{0}{x \times 1000}$
	| ExactMs  (v Int)         // yields $\rewriterate{x}{x}$
	| ExactS   (v Int)         // yields $\rewriterate{0}{x \times 1000}$
	| RangeMs  (v Int) (v Int) // yields $\rewriterate{x}{y}$
	| RangeS   (v Int) (v Int) // yields $\rewriterate{x \times 1000}{y \times 1000}$
\end{lstlisting}

\section{Interrupts}\label{lst:interrupts}
Most microcontrollers have built-in support for processor interrupts.
These interrupts are hard-wired signals that can interrupt the normal flow of the program to execute a small piece of code, the {ISR}.
While the ISRs look like regular functions, they do come with some limitations.
For example, they must be very short, in order not to miss future interrupts; can only do very limited {I/O}; cannot reliably check the clock; and they operate in their own stack, and thus communication must happen via global variables.
After the execution of the ISR, the normal program flow is resumed.
Interrupts are heavily used internally in the RTS of the microcontrollers to perform timing critical operations such as WiFi, I2C, or SPI communication; completed ADC conversions, software timers; exception handling; etc.

Interrupts offer two substantial benefits: fewer missed events and better energy usage.
Sometimes an external event such as a button press only occurs for a very small duration, making it possible to miss it due to it happening right between two polls.
Using interrupts is not a fool-proof way of never missing an event.
Events may still be missed if they occur during the execution of an ISR or while the microcontroller is still in the process of waking up from a triggered interrupt.
There are also some sensors, such as the CCS811 air quality sensor, with support for triggering interrupts when a value exceeds a critical limit.

\autoref{tbl:gpio_interrupts} shows the different types of interrupts.
\begin{table}
	\centering
	\caption{Overview of GPIO interrupt types.}%
	\label{tbl:gpio_interrupts}
	\begin{tabular}{ll}
		\toprule
		type    & triggers\\
		\midrule
		change  & input changes\\
		falling & input becomes low\\
		rising  & input becomes high\\
		low     & input is low\\
		high    & input is high\\
		\bottomrule
	\end{tabular}
\end{table}

\subsection{Arduino platform}
\autoref{lst:arduino_interrupt} shows an exemplatory program utilising interrupts written in Arduino's \Cpp{} dialect.
The example shows a debounced light switch for the built-in LED connected to GPIO pin 13.
When the user presses the button connected to GPIO pin 11, the state of the LED changes.
As buttons sometimes induce noise shortly after pressing, events within \qty{30}{\ms} after pressing are ignored.
In between the button presses, the device goes into deep sleep using the \cinline{LowPower} library.

\autoref{lst:arduino_interrupt:defs_fro} to \autoref{lst:arduino_interrupt:defs_to} defines the pin and debounce constants.
\autoref{lst:arduino_interrupt:state} defines the current state of the LED, it is declared \cinline{volatile} to exempt it from compiler optimisations because it is accessed in the interrupt handler.
\autoref{lst:arduino_interrupt:cooldown} flags whether the program is in debounce state, i.e.\ events should be ignored for a short period of time.

In the \cinline{setup} function (\autoref{lst:arduino_interrupt:setup_fro} to \autoref{lst:arduino_interrupt:setup_to}), the pinmode of the LED and interrupt pins are set.
Furthermore, the microcontroller is instructed to wake up from sleep mode when a \emph{rising} interrupt occurs on the interrupt pin and to call the ISR at \autoref{lst:arduino_interrupt:isr_fro} to \autoref{lst:arduino_interrupt:isr_to}.
This ISR checks if the program is in cooldown state.
If this is not the case, the state of the LED is toggled.
In any case, the program goes into cooldown state afterwards.

In the \cinline{loop} function, the microcontroller goes to low-power sleep immediately and indefinitely.
Only when an interrupt triggers, the program continues, writes the state to the LED, waits for the debounce time, and finally disables the \cinline{cooldown} state.

\begin{lstlisting}[numbers=left,label={lst:arduino_interrupt},caption={Light switch using interrupts in Arduino.}]
#define LEDPIN 13[+\label{lst:arduino_interrupt:defs_fro}+]
#define INTERRUPTPIN 11
#define DEBOUNCE 30[+\label{lst:arduino_interrupt:defs_to}+]

volatile int state = LOW;[+\label{lst:arduino_interrupt:state}+]
volatile bool cooldown = true;[+\label{lst:arduino_interrupt:cooldown}+]

void setup() {[+\label{lst:arduino_interrupt:setup_fro}+]
	pinMode(LEDPIN, OUTPUT);
	pinMode(INTERRUPTPIN, INPUT);
	LowPower.attachInterruptWakeup(
		INTERRUPTPIN, buttonPressed, RISING);
}[+\label{lst:arduino_interrupt:setup_to}+]

void loop() {[+\label{lst:arduino_interrupt:loop_fro}+]
	LowPower.sleep();
	digitalWrite(LEDPIN, state);
	delay(DEBOUNCE);
	cooldown = false;
}[+\label{lst:arduino_interrupt:loop_to}+]

void buttonPressed() {[+\label{lst:arduino_interrupt:isr_fro}+]
	if (!cooldown)
		state = !state;
	cooldown = true;
}[+\label{lst:arduino_interrupt:isr_to}+]
\end{lstlisting}

\subsection{\MTask{} language}
\autoref{lst:mtask_interrupts} shows the interrupt interface in mTask.
The \cleaninline{interrupt} class contains a single function that, given an interrupt mode and a GPIO pin, produces a task that represents this interrupt.
Lowercase variants of the various interrupt modes such as \cleaninline{change :== lit Change} are available as convenience macros.

\begin{lstlisting}[label={lst:mtask_interrupts},caption={The interrupt interface in mTask.}]
class interrupt v where
	interrupt :: (v InterruptMode) (v p) -> MTask v Bool | pin p

:: InterruptMode = Change | Rising | Falling | Low | High
\end{lstlisting}

When the mTask device executes this task, it installs an ISR and sets the rewrite rate of the task to infinity, $\rewriterate{\infty}{\infty}$.
The interrupt handler is set up in such a way that the rewrite rate is changed to $\rewriterate{0}{0}$ once the interrupt triggers.
As a consequence, the task is executed on the next execution cycle.

The \cleaninline{pirSwitch} function in \autoref{lst:pirSwitch} creates, given an interval in \unit{\ms}, a task that reacts to motion detection by a PIR sensor (connected to GPIO pin 0) by lighting the LED connected to GPIO pin 13 for the given interval.
The system lightens the LED again when there is still motion detected after this interval.
By changing the interrupt mode in this program text from \cleaninline{High} to \cleaninline{Rising} the system lights the LED only one interval when it detects motion no matter how long this signal is present at the PIR pin.

\begin{lstlisting}[caption={Example of a toggle light switch using interrupts.},label={lst:pirSwitch}]
pirSwitch :: Int -> Main (MTask v Bool) | mtask v
pirSwitch =
	declarePin D13 PMOutput \ledpin->
	declarePin D0 PMInput \pirpin->
	{main = rpeat (     interrupt high pirpin
	               >>|. writeD ledpin false
	               >>|. delay (lit interval)
	               >>|. writeD ledpin true) }
\end{lstlisting}

\subsection{Motion detection}
If a person enters the room, the temperature is bound to change faster.
Using the passive infrared (PIR) sensor, motion can be detected.
If the PIR detects motion, the GPIO pin it connects to will be high for a couple of seconds.
Polling this pin continuosly to check whether there is motion consumes a lot of energy.
Luckily, using an \emph{high} interrupt handler we get notified when the pin is high, i.e.\ when there is motion.

\begin{exercise}[H]
	Adapt the temperature monitor so that the temperature is only measured every minute.
	In addition, if motion is detected, record the motion and take an extra temperature measurement.

	\begin{enumerate}
		\item Adapt the temperature function so that it measures only once every minute (this is done using \cleaninline{temperature`}).
		\item Add the declaration of the PIR sensor to the top level of the \mTask{} task using the \cleaninline{PIR} functions:
			\lstinputlisting[firstline=36,lastline=36]{tempmon4.icl}
		\item Add an extra SDS to record the number of movements. E.g.%
			\lstinputlisting[firstline=15,lastline=15]{tempmon4.icl}
		\item Extend the main task using a parallel combinator so that it not only calls \cleaninline{tempfun} but also \cleaninline{motionfun}.
		\item Extend \cleaninline{motionfun} so that it also measures the temperature when detecting motion.
	\end{enumerate}
	
	\caption{Temperature monitor, Motion detection ({\rm\tt tempmon4.icl})}%
	\label{ex:motion_temperature_monitor}
\end{exercise}
