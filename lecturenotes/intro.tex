\section{Introduction}

The Internet of Things, IoT, is the collection on interconnected smart devices that senses and controls objects in the world.
It consists of a wide variety of nodes including smart thermostats, lightbulbs, doorbells, human presence sensors and electricity meters.
The number of IoT devices is huge.
It is estimated at 14.4 billion at the end of 2022 and is expected to grow to 27 billion connected devices at the end of 2027%
\footnote{\url{https://iot-analytics.com/number-connected-iot-devices} accessed \formatdate{11}{11}{2022}.}.
As a reference the world population is nearly 8 billion persons in 2022 and is expected to grow with a few percent over this period%
\footnote{\url{https://ourworldindata.org/world-population-growth} accessed \formatdate{11}{11}{2022}.}.

The number of nodes in the IoT is not only large, most of these nodes are also running 24/7.
By the nature of the IoT there is a significant amount of communication between the devices.
This makes it challenging to run the IoT in a sustainable way.
Fortunately there are some opportunities to limit the energy consumption of the IoT.
First, many tasks of the IoT nodes are rather simple.
This implies that we can use a small and energy efficient microcontroller based node instead of a solution that is based on a full microprocessor based computer, like a Raspberry Pi.
Second, we can use edge computing to limit the amount of communication.
For instance, a sensor reports only significant changes in it readings to a server instead of submitting each value.
Third, many microcontrollers have facilities to reduce the power consumption.
For instance they can switch of the WiFi-radio when there is temporally no need for communication.
When there is no need for any activity during some time the microcontroller can even switch to a low power sleep mode.
Finally, some sensors have the ability to wake up the microcontroller whenever required.
This prevents regular waking up of the microcontroller to check whether something interesting is happening.

On the positive side, many IoT applications help to save power by optimizing climate control systems in buildings, adaptive lightning, predictive maintenance to name just a few application areas.
There is obviously a balance in the power it takes to operate the IoT and the energy savings and increase of comfort that is provided by the IoT
This makes it worthwhile to ensure that IoT systems themselves are sustainable.

To give an impression of the power consumption of IoT nodes we list some figures.
A single-board microprocessor based computer like the Raspberry Pi 4 consumes about 4W when it is idle and 6W when its four cores are active. This is exclusive peripherals and WiFi communication.
This is not bad compared to the 60W consumed by the average \qty{15.6}{inch} laptop%
\footnote{\url{https://www.netbooknews.com/tips/how-many-watts-laptop-use/} accessed 14-Nov-2022}.
The popular ESP8266 microcontroller consumes a maximum of 0.5W during active WiFi communication.
When the WiFi is switched off, modem-sleep, this is reduced to 0.05W.
When the microcontroller takes a nap, light-sleep, it consumes only 0.001W%
\footnote{\url{https://www.espressif.com/sites/default/files/documentation/9b-esp8266-low_power_solutions__en.pdf} accessed \formatdate{11}{11}{2022}.}.
Moreover, a single board computer is typical about ten times more expensive than a simple microcontroller board.
There are microcontrollers that consume considerable less energy, but they are less common and usually more expensive.

Energy efficiency is obviously very important for battery operated IoT nodes.
However, due to the huge number of devices it is important for the entire IoT.
Operating 14.4 billion IoT nodes based on Raspberry Pi's for a year requires about 600TWh.
As a reference, the annual productions of the single Dutch nuclear power plant is 4TWh%
\footnote{\url{https://www.rijksoverheid.nl/onderwerpen/duurzame-energie/opwekking-kernenergie} accessed 15-Nov-2022}.
This indicates that operating the IoT takes a serious amount of energy and reducing the power demand of individual nodes makes the system more sustainable.

These numbers make it very attractive to build IoT nodes with microcontrollers rather than with full fledged computers based an a microprocessor like the Raspberry Pi.
However, there is a significant price to be paid.
First of all the microcontrollers are much slower and have far less memory than the microprocessor based systems.
As a consequence these IoT nodes run typically without a full operating system.
Often there is just the user program.
Sometime the user program is constructed with the help of a special purpose OS like {FreeRTOS}.
This makes programming IoT systems and updates of these programs very complicated.
Especially when several subtasks have to run on the IoT node.
Without OS there is no support for multithreading.
This implies that the user program has to interleave the subtasks.
Modem-sleep and light-sleep have to be controlled by the user program containing those subtasks.
The system can only go to a low power state when this is possible in all subtasks.

Adding such a node to the IoT is also challenging.
Due to the limitations of the microcontroller it is typically programmed in a dialect of C++ or Python.
For the communication it uses a protocol like MQTT on top of the normal internet protocols.
The server that has to cooperate with the IoT node is typically written in high level language like Java.
Since the IoT node has a very limited amount of memory the sensor reading will be stored in a database via the server.
The user interface of the program either runs in a browser, using HTML and for instance JavaScript or WebAssembly, or in a smartphone app for Android and or {iOS}.
The program on the IoT node also has to take care about its own updates since there is no OS and physical access to the node for an update is usually too demanding.
This implies that many programming languages, protocols and systems are involved in a single task of the IoT.
These differences in datatypes and interaction are known as \textit{semantic friction}.
In general, this makes the development of IoT applications cumbersome and its maintenance even more challenging.

\subsection{Tierless Program Stacks}

These problems can be prevented by generating all required software for the entire system from a single source.
Such tierless systems are known for web stacks, e.g. Links~\cite{cooper2006links} and Hop~\cite{serrano2006hop}.
These systems focus on web-pages and the associated servers.

In this paper we tierless system focussed on Task Oriented Programming, {TOP}.
As the name suggests this declarative formalism focusses on tasks.
These pieces of work are executed by humans or machines, including the edge nodes of the IoT.
Tasks are here either primitive tasks like reading a sensor, operating an actuator, and filling out a web-form or subtasks composed by combinators to more complex tasks.
For instance, we can compose tasks in parallel as an alternative for each other or subtasks that are both required without fixing their order.
Subtask can also be composed sequentially.
In contrast to ordinary computations, tasks can inspect the current value of subtasks.
They can decide to move on based on the current value of a subtask, even when that subtask is not finished yet.

The iTask system is the oldest implementation of TOP~\cite{TOP-ICFP07,TOP-PPDP12}.
It creates a web-server, the associated storage and tailor-made web-pages for all users of the system.
This web-interface guides the users through their current tasks.
To ensure good performance, a part of the task is executed in the browser rather than on the server.
The entire system is built as a shallowly embedded Domain-Specific Language, DSL, in the pure functional programming language \Clean~\cite{brus_clean_1987}.
This implies that the iTask system is a set of functions and associated datatypes that is fully integrated with \Clean.
The tasks can use all datatypes and computations of \Clean.
Each iTask program is a single piece of software in \Clean.
This implies that the compiler checks all types and definitions as usual.
The entire system with dynamic web-pages, storage control, the web-server and the required networking is generated from this single tierless source.

The TOP approach is also very suited to control the IoT edge devices.
They run tasks reading sensors, controlling peripherals and communicating with the servers.
However, the microcontrollers empowering these devices are not powerful enough to run the iTask-servers.
Since these edge devices need no own web-server nor very complex computations, it is also not necessary to run a full iTask server there.
The mTask system is created to solve this gap.
On one hand it an embedded DSL in \Clean{} that is fully integrated with iTask.
On the other hand the compiler can distinguish the iTask and mTask parts just enough to generate code that fits on the edge devices with very limited power.
Only the code needed to execute an mTask is dynamically shipped to the edge device that needs to execute that task.
Just like tasks in iTask, tasks in mTask can be dynamically created in the hosting programming language.
Each mTask is dynamically compiled to tailor-made bytecode.
This bytecode is shipped towards the edge device and executed by a tailor-made tiny operating system on the device.
Since everything is in the same source we obtain the best of both worlds.
The tierless approach ensures consistency of the entire software system, while the separate mTask DSL facilitates executions of distinguished parts of this system on cheap and energy-efficient hardware.

The tailor-made tiny operating system on the devices knows that it is executing mTask programs.
It can use this knowledge for smart scheduling.
The subtasks will only be scheduled after a complete rewrite step in the mTask {DSL}.
This implies that the subtasks are never interrupted during communication with the server or peripherals, nor while they are accessing shared memory.
This makes it easy to interleave subtasks efficiently.
Moreover, the feather-light OS knows what the tasks are doing.
This knowledge can be used to save energy.
For instance, the system can switch to a light-sleep mode when all subtasks are waiting in a delay.
When the system detects that some tasks is checking a temperature sensor in a high frequency, these sensor reading can be dealyed since it is unlikely that the temperature changes rapidly.
This delay can enable more execution time for more useful tasks or switching to sleep mode when there are no urgent tasks to be done.
The automatic delay of subtasks can be fine tuned within the mTask program whenever that is desired.
In a traditional tiered system it is much harder to achieve similar energy savings.
The programmer needs detailed knowledge of all subtask that will be present dynamically and schedule the sleep explicitly.
This is hard and error prone, especially when new versions of the software are required.


The iTask/mTask system enables sustainability in various different interpretations.
First, the tierless approach ensures programs that are much smaller and easier to maintain than traditional tiered programs~\cite{tiot}.
Second, the mTask addition to this TOP system enable us to use energy efficient and cheap microprocessor based hardware.
Finally, the optimized scheduling of mTasks saves energy by switching to sleep mode automatically. 

\subsection{Structure of the paper}

The next section introduces edge computing by the famous blink example.
\autoref{sec:dsl} explains the \mTask{} DSL and its implementation in the amount of detail required for  this paper.
For a more thorough overview of this language the reader is referred to~\cite{koopman_task-based_2018,LubbersMIPRO,lubbers_writing_2019,lubbers_interpreting_2019,lubbers20tiered}.
In \autoref{sec:green} we explain how the interpreter can save energy by inspecting the current tasks.
When these subtasks can be delayed, the entire system goes to a sleep mode to save energy.
We can save even more energy when we replace polling of sensors by interrupts.
The system can be sleeping until the ineterrupt wakes it up.
In \autoref{sec:conclusion} we draw conclusions. 

Appendix~\ref{sec:software} explains how the software used in this paper can be installed.
It is required to make the assignments provided in this paper.
Appendix\ref{sec:measruringPower} shows a way to measure the actual poser consumption of a microprocessors with an appropriate sensor and another microprocessor.
This can be used to verify that techniques discussed here actually decrease the amount of energy used. 
The solutions to the exercises are provided in Appendix~\ref{sec:solutions}.

%\subsection{Sustainable IoT computing}
%Internet of Things
%
%Green IoT techniques
%
%\subsection{Task-oriented programming}
%TOP in general
%
%\subsection{TOP for the IoT}
%General overview of \mTask{}
%
%General overview of iTask
%
%Explain why TOP is so nice for the IoT
